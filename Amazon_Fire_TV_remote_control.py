#How to Automate Fire TV Remote Control Navigation

#As we have already discussed, to test Fire TV apps, you must automate remote control navigation within the app.

#The following is an example of how to use the pressKey() method for setting up functions for every action on the Amazon Fire TV remote control:


def up(self):
        '''
        KEYCODE_DPAD_UP
 
        Value: 19
        '''
        self.driver.press_keycode(19)
        print("   + Navigate Up")
    
    def down(self):
        '''
        KEYCODE_DPAD_DOWN
 
        Value: 20
        '''
        self.driver.press_keycode(20)
        print("   + Navigate Down")
    
    def left(self):
        '''
        KEYCODE_DPAD_LEFT
 
        Value: 21
        '''
        self.driver.press_keycode(21)
        print("   + Navigate Left")
    
    def right(self):
        '''
        KEYCODE_DPAD_RIGHT
 
        Value: 22
        '''
        self.driver.press_keycode(22)
        print("   + Navigate Right")
 
    def select(self):
        '''
        KEYCODE_DPAD_CENTER
 
        Value: 23
        '''
        self.driver.press_keycode(23)
        print("   + selected")
    
    def home(self):
        '''
        KEYCODE_HOME
 
        Value: 3
        '''
        self.driver.press_keycode(3)
        print("   + Press Home")
    
    def back(self):
        '''
        KEYCODE_BACK
 
        Value: 4
        '''
        self.driver.press_keycode(4)
        print("   + Press Back")
    
    def menu(self):
        '''
        KEYCODE_MENU
 
        Value: 82
        '''
        self.driver.press_keycode(82)
        print("   + Press Menu")
    
    def fast_forward(self):
        '''
        KEYCODE_MEDIA_FAST_FORWARD
 
        Value: 90
        '''
        self.driver.press_keycode(90)
        print("   + Press Fast Forward")
    
    def play_pause(self):
        '''
        KEYCODE_MEDIA_PLAY_PAUSE
 
        Value: 85
        '''
        self.driver.press_keycode(85)
        print("   + Press Play Pause")
    
    def rewind(self):
        '''
        KEYCODE_MEDIA_REWIND
 
        Value: 89
        '''
        self.driver.press_keycode(89)
        print("   + Press Rewind")

