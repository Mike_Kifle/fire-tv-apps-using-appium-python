from appium import webdriver
 
driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_capabilities={
    'platformName': 'Android',
    'deviceName': 'Fire TV',
    'appPackage': 'com.netflix.ninja',
    'appActivity': 'com.netflix.ninja.ui.splashscreen.SplashScreenActivity'
})
 
driver.find_element_by_id('com.netflix.ninja:id/login_button').click()
 
driver.find_element_by_id('com.netflix.ninja:id/username_edit_text').send_keys('your_username')
 
driver.find_element_by_id('com.netflix.ninja:id/password_edit_text').send_keys('your_password')
 
driver.find_element_by_id('com.netflix.ninja:id/login_button').click()
